from earth import app as earth_app
from mars import app as mars_app
from importlib import import_module
from conffu import Config
from os import chdir
from pathlib import Path
from hypercorn.middleware import DispatcherMiddleware

# set working directory to the folder this script is in
chdir(Path(__file__).resolve().parent)

cfg = Config.from_file('apps.json')

app_defs = {}
for app in cfg.apps:
    if 'disabled' in app and app.disabled:
        continue

    mod = import_module(app.name)
    mod_app = getattr(mod, app.app)
    globals().update({f'{app.name}_app': mod_app})
    app_defs[app.path] = mod_app

apps = DispatcherMiddleware(app_defs)
