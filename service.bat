@echo off

SET VENV=C:\dev\env\iis_quart

CALL %VENV%\Scripts\activate.bat
hypercorn dispatcher:apps
