# IIS Quart

Template project, providing a starting point for setting up Quart on an IIS instance.

## Setup

Assuming IIS has been installed and configured to run on a suitable Windows server:

- Install Python on the server<br/>
  The installation should be in a location accessible from the web server, does not need to be on the path (in fact, it's likely preferable that it isn't).
- Get the project onto the server<br/>
  Either `git clone https://gitlab.com/Jaap.vanderVelde/iis-quart.git` or download the project from the Git project.
- Create a virtual environment, install requirements from the project<br/>
  `pip install -r requirements.txt`
- Update `service.bat` to load your virtual environment before starting the server<br/>
  It should be enough to update `SET VENV=<path to your virtual environment>`
- Use nssm in [bin](bin) to install 'service.bat' as a service<br/>
  `nssm install iis_quart service.bat`</br>
  You can use a different name from `iis_quart`, or even start `nssm install` to use the GUI for installation.
  
For removal later, run `nssm remove iis_quart confirm`.

The example configuration in [apps.json](apps.json) sets up two simple Quart apps, on URLs http://localhost:8000/earth and http://localhost:8000/mars

For each site, in ISS set up a reverse proxy configuration; more info here https://docs.microsoft.com/en-us/iis/extensions/url-rewrite-module/reverse-proxy-with-url-rewrite-v2-and-application-request-routing 
An example [web.config](iis/web.config) is provided.

## Add additional apps

- Creating new package folders for them similar to `earth` and `mars
- Including them in [apps.json](apps.json). 
- Update [requirements.txt](requirements.txt) with a link to the package requirements. 
- Install the requirements in the virtual environment.
- Restart the service and update your IIS configuration accordingly (if they run on different domain, for example).
